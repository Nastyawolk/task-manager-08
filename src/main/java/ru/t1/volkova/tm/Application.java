package ru.t1.volkova.tm;

import ru.t1.volkova.tm.constant.*;
import ru.t1.volkova.tm.model.Command;
import ru.t1.volkova.tm.repository.CommadRepository;
import ru.t1.volkova.tm.util.FormatUtil;

import java.util.Scanner;

public class Application {

    public static void main(final String[] args) {
        processArguments(args);
        System.out.println("** WELCOME TASK MANAGER **");
        final Scanner scanner = new Scanner(System.in);
        while (!(Thread.currentThread().isInterrupted())) {
            System.out.println("ENTER COMMAND");
            final String command = scanner.nextLine();
            processCommand(command);
        }
    }

    private static void processArguments(final String[] args) {
        if (args == null || args.length == 0) {
            return;
        }
        processArgument(args[0]);
        exit();
    }

    private static void processArgument(final String arg) {
        if (arg == null || arg.isEmpty()) return;
        switch (arg) {
            case ArgumentConst.ABOUT:
                showAbout();
                break;
            case ArgumentConst.VERSION:
                showVersion();
                break;
            case ArgumentConst.HELP:
                showHelp();
                break;
            case ArgumentConst.INFO:
                showInfo();
                break;
            case ArgumentConst.ARGUMENTS:
                showArguments();
                break;
            case ArgumentConst.COMMANDS:
                showCommands();
                break;
            default:
                showArgumentError();
                break;
        }
    }

    private static void processCommand(final String command) {
        if (command == null || command.isEmpty()) return;
        switch (command) {
            case CommandConst.ABOUT:
                showAbout();
                break;
            case CommandConst.VERSION:
                showVersion();
                break;
            case CommandConst.HELP:
                showHelp();
                break;
            case CommandConst.INFO:
                showInfo();
                break;
            case CommandConst.EXIT:
                exit();
                break;
            case CommandConst.ARGUMENTS:
                showArguments();
                break;
            case CommandConst.COMMANDS:
                showCommands();
                break;
            default:
                showCommandError();
                break;
        }
    }

    public static void showInfo() {
        System.out.println("[INFO]");
        final int availableProcessors = Runtime.getRuntime().availableProcessors();
        System.out.println("Available processors (cores): " + FormatUtil.formatBytes(availableProcessors));
        final long freeMemory = Runtime.getRuntime().freeMemory();
        System.out.println("Free memory: " + FormatUtil.formatBytes(freeMemory));
        final long maxMemory = Runtime.getRuntime().maxMemory();
        final String maxMemoryFormat = FormatUtil.formatBytes(maxMemory);
        final boolean maxMemoryCHeck = maxMemory == Long.MAX_VALUE;
        final String maxMemoryValue = maxMemoryCHeck ? "no limit" : maxMemoryFormat;
        System.out.println("Maximum memory: " + maxMemoryValue);
        final long totalMemory = Runtime.getRuntime().totalMemory();
        System.out.println("Total memory: " + FormatUtil.formatBytes(totalMemory));
        final long usageMemory = totalMemory - freeMemory;
        System.out.println("Usage memory: " + FormatUtil.formatBytes(usageMemory));
    }

    public static void exit() {
        System.exit(0);
    }

    private static void showArgumentError() {
        System.err.println("[ERROR]");
        System.err.println("This argument is not supported...");
        System.exit(1);
    }

    private static void showCommandError() {
        System.err.println("[ERROR]");
        System.err.println("This command is not supported...");
        System.exit(1);
    }

    private static void showAbout() {
        System.out.println("[ABOUT]");
        System.out.println("name: Anastasia Volkova");
        System.out.println("e-mail: aavolkova@t1-consulting.ru");
        System.out.println("e-mail: nastya-wolk@mail.ru");
    }

    private static void showVersion() {
        System.out.println("[VERSION]");
        System.out.println("1.8.0");
    }

    public static void showCommands() {
        System.out.println("[COMMANDS]");
        final Command[] commands = CommadRepository.getTerminalCommands();
        for (final Command command : commands) {
            if (command == null) continue;
            final String name = command.getName();
            if (name == null || name.isEmpty()) continue;
            System.out.println(name);
        }
    }

    public static void showArguments() {
        System.out.println("[ARGUMENTS]");
        final Command[] commands = CommadRepository.getTerminalCommands();
        for (final Command command : commands) {
            if (command == null) continue;
            final String argument = command.getArgument();
            if (argument == null || argument.isEmpty()) continue;
            System.out.println(argument);
        }
    }

    private static void showHelp() {
        System.out.println("[HELP]");
        final Command[] commands = CommadRepository.getTerminalCommands();
        for (final Command command : commands) System.out.println(command);
    }

}
