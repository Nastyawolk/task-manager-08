package ru.t1.volkova.tm.model;

import ru.t1.volkova.tm.constant.ArgumentConst;
import ru.t1.volkova.tm.constant.CommandConst;

public class Command {

    private String name;

    private String argument;

    private String desription;

    public Command() {
    }

    public Command(String name, String argument, String desription) {
        this.name = name;
        this.argument = argument;
        this.desription = desription;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getArgument() {
        return argument;
    }

    public void setArgument(String argument) {
        this.argument = argument;
    }

    public String getDesription() {
        return desription;
    }

    public void setDesription(String desription) {
        this.desription = desription;
    }

    @Override
    public String toString() {
        String displayName = "";
        if (name != null && !name.isEmpty()) displayName += name;
        if (argument != null && !argument.isEmpty()) displayName += ", " + argument;
        if (desription != null && !desription.isEmpty()) displayName += ": " + desription;
        return displayName;
    }

}
